Ce billet à été inspiré par l'article de [Lokoyote](https://lokoyote.eu/du-markdown-pour-de-la-prise-de-note/).

![Md](https://upload.wikimedia.org/wikipedia/commons/4/48/Markdown-mark.svg)

## Mais qu'est-ce que c'est quoi dis donc ?

Le Markdown (Md) est un langage de mise en forme. 

>  Euh attends, tu veux dire mise en page ?

Non ! Pas du tout ! À ne pas confondre avec de la mise en page, rien à voir ! La mise en page c'est les caractéristiques d'un texte (où d'une image) relatif à un format, une taille. Le A4 par exemple.

La mise en forme c'est du texte relatif à... lui même !

> Gnin ?

Et ça c'est super pratique ! Plus besoin de se prendre la tête, je définis les différents composants de mon texte, et basta !
> Si tu veux jeter un coup d'oeil à ce à quoi ça ressemble, voici la [syntaxe](https://fr.wikipedia.org/wiki/Markdown). Sache que cet article est rédigé en Markdown.

## Les avantages

* #### Rapidité rédactionnelle

Avec un simple petit ~~(code)~~ caractère je peux signifier un titre, une citation, un lien, une liste, … C'est simple à utiliser, et très fluide (surtout avec un clavier [bépo](https://bepo.fr/wiki/Accueil) ^^). En bref le markdown c'est [KISS](https://fr.wikipedia.org/wiki/Keep_it_Simple,_Stupid) !

On peut facilement rédiger un script, des notes, un récit, un article où un scénario, c'est polyvalent.

* #### Lisibilité

Comme la syntaxe est toujours la même, je repère très vite quelles sont les informations que je recherche.

* #### Simplicité

Il y a ce qu'il faut pour être compris, pas moins, pas plus. C'est sobre et efficace ! En une heure on fait le tour de la codification, et à la fin de la journée on le sait par cœur.

* #### Portabilité

Autre gros avantage, et pas des moindres, c'est la portabilité ! Ce langage est tellement plébiscité qu'il se retrouve à peu près partout. Je l'utilise pour :

* La rédaction avec [Atom](https://atom.io/), [Typora](https://typora.io/), [Remarkable](https://remarkableapp.github.io/), [Abricotine](http://abricotine.brrd.fr/), … Y'a du choix !
> J'utilisais Atom, mais pour ce que je faisais c'est un peu comme se brosser les dents avec un balai, du coup grâce à Lokoyote je découvre Typora (avec lequel je rédige ce billet) qui permet notamment une preview live du texte super pratique (les autres proposent la même chose, mais dans une fenêtre différente). Simple et efficace.

* Les présentations de type diaporama avec [Reveal.js](https://github.com/hakimel/reveal.js) ou [Marp](https://yhatt.github.io/marp/).
> Ah ! Ça c'est vraiment cool ! Voici un [exemple](http://guepe.ateliez.fr/revealjs/tdr.html) de présentation avec Reveal.js et si tu veux en savoir plus je t'invite à lire les tutos de [nlQnutn](http://guepe.ateliez.fr/shaarli/?zCopaw) sur le sujet, ils sont clairs et bien fait. Reveal.js est très puissant et très agréable avec une simple feuille Markdown. Pour les utilisateurs avancés les possibilités sont énormes en personnalisation de templates.
> ***
> Marp est très différent, il sort un rendu PDF d'un fichier Markdown. Il est par conséquent beaucoup plus simple et sobre que Reveal.js, il a beaucoup moins de fonctionnalités. Il fait le TAF et est très facile à prendre en main, n'y a t-il pas que ça qui compte ?

* Le CMS [PluXml](http://www.pluxml.org/) Super moteur de blog avec le plugin [Markdown parser](http://warriordudimanche.net/?article0351/markdown-parser-du-markdown-minimaliste-pour-pluxml) de [Bronco](http://warriordudimanche.net/). Sinon [voici]( http://guepe.ateliez.fr/shaarli/?searchtags=cms+libre+markdown+) une liste d'autres CMS libres qui fonctionnent avec Markdown.
* Le [Shaarli](https://www.shaarli.fr/redirect.php), outil génialissime, un archiveur de lien, qui m'est aujourd'hui indispensable.
* [KanBoard](https://kanboard.net/) pour l'organisation.
* [BoZoN](http://bozon.pw/fr/) (le super cloud de Bronco) gère nativement le Markdown, pratique pour le partage et l'édition en ligne d'un fichier.
* [GitHub](https://github.com/) et [GitLab](https://about.gitlab.com/) utilisent tout deux le un certain type de Markdown pour la rédaction de leurs articles et de la documentation.
* N'oublions pas [Diaspora](https://fr.wikipedia.org/wiki/Diaspora*), le réseau social décentralisé !

De plus le Markdown c'est juste un fichier texte, éditable par n'importe quel traitement de texte ou bloc-notes. Donc, en gros, modifiable n'importe où. Il est donc indépendant de tout logiciel ou de système d'exploitation. 

Un des objectifs de Markdown était de pouvoir se convertir facilement en html. Mais il est aussi facilement convertible vers d'autres formats. Je vais donc te parler rapidement de [Pandoc](http://pandoc.org/), un logiciel libre très efficace de conversion de documents. Il est assez magique. Il possède d'ailleurs sa propre syntaxe de Markdown plus riche que la version de base !

À vrai dire il ne me manque plus qu'un plugin pour [Dokuwiki](https://www.dokuwiki.org/start?do=search&id=markdown) et je serais comblé ^^ En fait 2 existent, mais ils ne sont plus maintenus...

Et je crois qu'il est même possible de faire du [mindmap](http://guepe.ateliez.fr/shaarli/?wwwvBw) avec Markdown en utilisant Atom, ça serait ouf, faudra que j'essaie un de ces jours !

## Un inconvénient peut-être ?

Essayons tout de même d'être objectif ! Oui le Markdown c'est génial, mais non ça ne fait pas de mise en page. C'est sobre, peut-être même austère. Ce n'est pas fait pour l'impression par exemple, et les tableaux sont très limités.  Il existe d'autres variantes du Markdown, plus poussées, qui permettent de combler quelques lacunes (voir [Pandoc](http://pandoc.org/)).

En résumé, c'est un outil. Il ne convient pas à tout le monde, mais bien utilisé, il s'avère redoutable !

***
> ❝Un langage unique pour le Troll des Ténèbres sur son sombre clavier,

> Au pays des Internets où s'étendent les ombres

> Un langage pour les gouverner tous

> Un langage pour les trouver

> Un langage pour les amener tous,

> Et dans les ténèbres les lier !❞

***

