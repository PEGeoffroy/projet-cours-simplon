# Angular 

**Version 6** (la 7 sort en septembre/octobre 2018), il est écrit en TypeScript (Microsoft). Angular influence le TS. :warning: AngularJS n'est pas Angular :warning:

> Framework JS

MVW/C (Whatever)

Developpé par google, mais libre et open-source.

Ancêtre : Angular JS

[MEAN](http://mean.io/) Stack : Mongo Express Angular Node

:warning: NgModule (AppModule) c'est le cœur de angular, il faut venir déclarer les components et les services, on va pouvoir déclarer d'autres modules qui auront leurs propres component et service.

Les components angular sont plus ou moins l'équivalent des controller symfony.

En angular chaque morceau de la page sont des components (header, menu, form, ...) contrairement à symfony ou on a une logique controller par page.

Le but c'est la réutilisabilité des petits bout de controller des components. Ça se rapproche des `embeded controller`.

Une class component comprend :

* des variables
* des methodes
* selecteur
* template

> src/app/app.module.ts // Il est lié au src/main.ts ligne 11 :

```typescript
platformBrowserDynamic().bootstrapModule(AppModule) // Bootstrap => demarage
bootstrap: [AppComponent] // app.module.ts Bootstrap => demarage
```

```typescript
@NgModule // C'est un décorateur
```

Les décorateurs c'est un peu comme les annotations.

```html
{{maVariale}}
```

***

Pour généner un nouveau component via le shell :

```sh
ng generate component monComponent
ng g c monComponent
ng new monProjet
```

```sh
ng serve
ng s
```

## Template

databinding : soit aller du parent à l'enfant au parent, l'inverse (unidirectionnel), ou les deux (bidirectionnel).

![databinding](images/databinding.png)

***

![two_way_data_binding](images/two_way_data_binding.png)

*propriété ou plutôt valeur

Encapsulation, chacun possède ses données ?

Les attributs sont appellé des directives, il y en a plusieurs types :

* celle entre crochet : la valeur de la directive provient de la classe component vers le template.
* celle entre parenthèse : concerne les evenement, du template vers le component.

```html
(ngSubmit)
```

* [()] : bidirectionnel, c'est une `banana in a box`

```html
<input type="text" [(ngModel)]="texte">
<p>{{texte}}</p>
```

* `*`  : C'est une directive avec un impact/changement structurel (if, boucle, ...)

```html
<p *ngIf="tableau.length > 3">There are more than 3 elements</p>

<p *ngFor="let item of tableau">
  {{item}}
</p>
```

[ngIf](https://angular.io/api/common/NgIf)

Le *ngFor est à mettre sur le truc qui est répété.

On peut aussi mettre des directives (conditions/boucles) sur des components.

On ne peut mettre qu'une directive par balise, à part si on utilise ng-container !

Jquery est une librairie, Angular est un framework.

***

Angular peut être livré de 2 façon différentes :

* Compilation **[just-in-time](https://en.wikipedia.org/wiki/Just-in-time_compilation)** (transpilation du TS) -> developpement (lourd pour les clients)
* Compilation **[ahead-of-time](https://fr.wikipedia.org/wiki/Compilation_anticip%C3%A9e)**, on va tout compiler, il prend tout les template et les convertis

***

La fausse gestion des pages dans une appli one-page s'appelle "state".

***

slug : url/:id

***

3 façons d'intéragir avec les template d'angular :

* les balises : <router-outlet></router-outlet>
* Les directives
* Trucs *

***

Interface et pas entité

```ts
typeof(Number) !== typeof(number)
```

binder = associer

http://localhost:8080/total-output

On a pas le droit de donner une valeur à une propriété dans une interface.

***

:warning: Litteral d'objet :

new Array()

[]

***

Observable : objet dont on peut voir l'état

***

## Montage

On dit d'un component qu'il est monté lorsqu'il est présent dans le DOM ?

## Scope

1. Quand on déclare une variable dans un bloc, elle n'existe que dans ce bloc, et ses enfants.
2. Un bloc a accès aux variables du bloc parent.
3. Le bloc parent n'a pas accès aux variables déclarées du bloc enfant.

:warning: Distinction entre assignation et déclaration

## Component

![angular_component](images/angular_component.png)

## Observable

flux

Promise (then, catch) :arrow_right: Observable (subscribe (données, erreur, complete))

pipe :arrow_right: modification de l'observable (serie de traitement)

* opérateurs :arrow_right: fonctions dédiées aux observables
  * map :arrow_right: modifie la valeur conterue dans l'observable

Deux type de pipes y'en a un dans le template {{ toto | json }}

## Injection

Injection de dépendences.

## Aide

![databinding](images/chemin.png)

> Illustration de Berta M.

## Liens

* [Router](https://angular.io/guide/router)