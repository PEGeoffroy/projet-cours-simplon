# Webpack

C'est un packer, c'est à dire qu'il optimise (minify) et qu'il rend plus leger nos fichier.

Il module les fichiers js et organise le code (secondaire).

> Écrire plusieurs fichiers spécifique permet organiser son code.

Il fait **exporter** les class et les dépendances. on fait une déclaration dans le module pour le rendre disponible dans d'autres pages.

Ensuite on **importe** un seul fichier (bundle) js dans notre html.

> Arbre des dépendances ?

La minification permet d'enlever tout les caractères inutiles, il renomme toutes les fonctions et les variables, le code devient presque illisible.

Tant que c'est du front, webpack gère tout.

Webpack utilise des loaders (plugin). Pour travailler avec les loaders, on le lui precise dans la configuration webpack.

## Liens

* [Entry points](https://webpack.js.org/concepts/entry-points/)

## Culture

> [parcel](https://fr.parceljs.org/)

```js
module.exports = {
  mode: 'development',
  entry: './src/js/main.js',
  output: {
    filename: 'bundle.js'
  },
  devtool: 'inline-source-map',
  module: {
    rules: [{
      test: /\.s?css$/,
      use: [
        "style-loader", // creates style nodes from JS strings
        "css-loader", // translates CSS into CommonJS
        "sass-loader" // compiles Sass to CSS
      ]
    }]
  }
};
```