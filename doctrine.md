# Doctrine (ORM)

## Principes

Doctrine est un ORM (Objet Relationnal Mapping), utilisé par défaut par Symfony. Doctrine permet de ne plus faire de [SQL](https://en.wikipedia.org/wiki/SQL), il fait un lien direct entre les classes et la bdd.

> [Projet GitLab](https://gitlab.com/simplonlyon/P6/symfony-orm)
>
> [Projet perso](https://gitlab.com/PEGeoffroy/symfony-orm)

***

Pour stocker les variables d'environement si c'est une information sensible on le met dans le .env, sinon, tout ce qui peut etre exposé dans le .end.dist

***

```sh
php bin/console make:entity
php bin/console make:migration
php bin/console doctrine:migrations:migrate

php bin/console doctrine:fixtures:load
php bin/console doctrine:database:drop --force
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load
```

***

Dans VSCode :

> Ctrl-shift-P

Choisir : **Docker : Attach Shell**

Puis : **symfonyorm_php-form**

***

Si problème de driver :

```sh
In AbstractMySQLDriver.php line 125:
  An exception occurred in driver: could not find driver
In PDOConnection.php line 50:
  could not find driver
In PDOConnection.php line 46:
  could not find driver
```

Faire :

```sh
sudo apt install php7.2-mysql
```

Aller dans config/packages/doctrine.yaml

## Sources & notes

* [Wikipédia](https://fr.wikipedia.org/wiki/Doctrine_(ORM))
* [Eloquent](https://laravel.com/docs/5.4/eloquent) : ORM de Laravel

Beaucoup de langages orienté objet ont un ou plusieurs ORM, ils fonctionnent généralement plus ou moins de la même manière.