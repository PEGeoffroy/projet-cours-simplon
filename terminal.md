# Terminal

## Astuce

* Utiliser **tabulation** pour faire l'auto-complétion.
* Utiliser les flèches directionnelles **haut** et **bas** pour naviguer dans les dernières commandes.
* ^d : Quitter le terminal.
* q : Permet de quitter le processus
* Le tilde s'écrit aussi ~

## Ligne de commande

### cd : change directory

```terminal
cd dossierEnfant
```

Permet à l'utilisateur de changer la localisation du terminal.

```terminal
cd /
```

Retourne à la racine.

```terminal
cd ~
```

Retourne à home.

```terminal
cd ..
```

Retourne dans le dossier parent.

```terminal
cd ../..
```

Retourne 2x en arrière.

### ls : list

```terminal
ls Documents
```

Affiche ce qui est présent dans le dossier : dossiers, fichiers (executable)

```terminal
ls -a
```

Affiche tout, y compris les fichiers caché.

```terminal
ls -al
```

Fait une liste verticale du contenu du dossier, avec beaucoup d'information.

```terminal
ll
```

Raccourcis ?

### rm : remove

```terminal
rm monFichier
```

Supprime un fichier, et **un seul**.

```terminal
rmdir monDossier
```

Supprime un dossier vide.

```terminal
rm -rf
```

Suppression recursive d'un dossier et de tout son contenu (rf : récursif et force).

### apt : advanced packaging tool

```terminal
apt install
```

```terminal
apt remove
```

```terminal
apt update
apt upgrade
```

```terminal
apt list --upgradable
```

### man : le manuel