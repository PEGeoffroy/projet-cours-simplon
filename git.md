# Git : gestionnaire de versioning

1. [Introduction](#introduction)
2. [Plateformes](#plateformes)
3. [Commandes](#commandes)
4. [Gitignore](#gitignore)
5. [Méthodologie](#méthodologie)
6. [Préparation](#préparation)
7. [Usage](#usage)
8. [/!\ Liens utiles](#-liens-utiles)

## Introduction

Git est un gestionnaire de versioning permettant de :

* partager du code
* travailler collaborativement
* avoir un historique des modifications
* avoir des dépôts disant sur des serveurs.

*(Schéma des branches bienvenue !)*

> Il en existe d'autres comme [svn](https://fr.wikipedia.org/wiki/Apache_Subversion) et [mercurial](https://www.mercurial-scm.org/).

***

## Plateformes

* [GitHub](https://github.com/) est une plateforme qui offre un service git.
* [GitLab](https://gitlab.com/) est un logiciel libre que l'on peut installer chez soi, et c'est aussi une plateforme [à l'instar](https://fr.wiktionary.org/wiki/%C3%A0_l%E2%80%99instar_de) de GitHub.
* [Gogs](https://gogs.io/) est plus léger que ses deux compères.

Ils permettent de gérer les projets de manière plus simple.

## Commandes

```terminal
git init
```

Initialise le dossier en dépôt git local.

```terminal
git config --global user.email "mail@domaine.com"
git config --global user.name "name"
```

Configuration indispensable pour utiliser git en ligne de commande. À renseigner à l'identique de la plateforme GitLab pour pouvoir pusher directement sur le serveur.

```terminal
git clone monAdresse
```

Permet de cloner un dépôt distant depuis la plateforme git.

```terminal
git status
```

Renseigne le statut du dépôt, à savoir les modifications en attente de validation dans le working directory.

```terminal
git log
```

Affiche les commits.

```terminal
git branch maBranche
```

Crée une nouvelle branche appelé maBranche. Ou l'écrase si elle existait déjà.

```terminal
git checkout
```

re-initialise le fichier tel qu'il était dans le dépôt.

```terminal
git checkout maBranche
```

Permet de changer de branche.

```terminal
git add
```

Ajoute la modification au staging area.

```terminal
git commit -m "message de commit"
```

Ajoute les modifications au dépôt.

```terminal
git pull maRemote maBranche
```

Télécharge les modifications depuis mon dépôt distant sur ma branche.

```terminal
git push maRemote maBranche
```

Publie les modifications locales sur le dépôt distant.

```terminal
git merge
```

Fusionne la branche appelé avec celle sur laquelle on se trouve.

```terminal
git remote add maNouvelleRemote monDépôtDistant.git
```

Donne un surnom à mon dépôt distant !

```shell
git remote set-url origin monProjet.git
```

Change l'url du dépôt distant.

```shell
git clone monProjet.git nouveauNomDeProjet
```

Renomme le projet cloné.

## .gitignore

> Le .gitignore permet de ne pas balancer les trucs genre fichier sensible,librairies indésirables, les dossiers lourd comme le 'dist' ou le 'node_modules' (webpack).

## Méthodologie

> Travail en groupe

![Schema : staging -> dépôt](./images/git-add-commit-image.png)

![Schema : principle](./images/git-principle.png)

### Préparation

> Faire une liste des issues dans le board et leurs donner un poids.

1. Créer un dépôt distant commun (`/!\ interdiction de pusher ou de modifier directement ce repository /!\`).
2. Forker chacun le projet.
3. Cloner le dépôt en local à partir du fork.
4. Ajouter une remote pour le dépôt commun ('main' éventuellement) qui servira à récupérer (`git pull`) les modifications.

### Usage

1. Créer une branche par issue/sprint, et s'y rendre.
2. Faire ses modifications/ajouts.
3. Merger sa branch avec sa master
4. Pusher sa master sur le notre dépôt personnel distant.
5. Faire une merge request de sa master à la master du dépôt principal.

> Ne pas oublier de puller si il y a eu des changement sur le dépôt commun, puis résoudre les conflits, re-pusher et re-proposer la merge request.

## /!\ Liens utiles

* [Wikipédia](https://fr.wikipedia.org/wiki/Git)
* [Cheatsheet](https://www.git-tower.com/blog/git-cheat-sheet)
* [Git-flow cheatsheet](https://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html)
* [Learn Git Branching](https://learngitbranching.js.org/)
* [Git - petit guide](https://rogerdudler.github.io/git-guide/index.fr.html)
* [Git-game](https://github.com/git-game/git-game)