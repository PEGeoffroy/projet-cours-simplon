# POO et class

## Objet

### Définition

Un objet est une représentation sous forme de code, d'un concept physique ou non, défini par des **propriétés** et des **méthodes**. Les propriétés sont les variables des objets et les méthodes en sont les fonctions. Le js est majoritairement composé d'objets.

### Utilité

1. Les objets peuvent interagir entre eux.
2. On utilise les objets pour organiser son code, mais ils n'optimisent pas le code d'un point de vue performance.

## Class

Une classe est un schéma/moule avec lequel on créé des objets.

> Un objet est une instance de classe.
***
> Une instance de class est un objet créé avec cette même class.

### Représentation

|  Stylo            |
| :---------------: |
|  taille : number  |
| couleur : string  |
|   type : string   |
|       —————       |
| écrire() |

> Ceci est une représentation d'un modèle de class en [UML](https://fr.wikipedia.org/wiki/UML_(informatique)). Voir [StarUml](http://staruml.io/).

### Exemple

```js
class NomClass{
  constructor(param1, param2){
    this.prop1 = param1;
    this.prop2 = param2;
  }

  myMethod(){
    console.log(`${this.param1}, ${this.param2}`)
  }
}
```

## Convention

1. Chaque mot composant le nom de la class doit commencer avec une majuscule. [-> convention](https://wprock.fr/blog/conventions-nommage-programmation/).
2. On écrit chaque nouvelle class dans un fichier dédié (si on utilise webpack il ne faut pas oublier de l'exporter).

## Déclaration d'un objet sans class

```js
let firstObject = {
    property1: "valeur1",
    property2: true,
    secondObject: {
      property3: "valeur3"
 },
    numb1: 1,
    method1: function(){
        console.log("Patate !");
    },
    method2(){
      console.log("Carotte !");
    }
};

firstObject.property1 = "valeur2";

console.log(firstObject.secondObject.property3);
firstObject.method1();
```

```js
class MaClasse {
    //Constructeur - méthode de l'objet class appelé avec : monObjet  = new Maclasse(param);
  constructor(param1 = 'blue', param2 = 30, param3 = 'marqueur'){
    this.property1 = param1;
    this.property2 = param2;
    this.property3 = param3;
  }
    //Propriété
  write(text){
    let p = document.createElement('p');
    p.textContent = text;
    p.style.color = this.property1;
    p.style.fontSize = `${this.property2}px`;
  }
}
```

Pour acceder aux propriétés de notre de notre objet en son sein on utilise this.

Les fonctions ont des mots-clefs 'this' qui empêchent de faire référence aux this de notre class, on utilise donc la variable : that. [-> voir l'article complet sur la MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Op%C3%A9rateurs/L_op%C3%A9rateur_this). C'est pas ouf comme pratique donc il vaut mieux utiliser la [fat arrow](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Fonctions/Fonctions_fl%C3%A9ch%C3%A9es).

***

```js
{ } === new Object() // Tout est dépendant de cet objet
```

> À détailler

***

```js
class User {
    constructor(name, birthdate, gender = 1){
        this.name = name; // "name" est le paramètre de la fonction et "this.name" est la propriété de la class, ils sont différent !!! Là on dit maPropriétéDeLInstance = monParamètreDeFonction
    }
    sayHello(){
        console.log(`Hello, my name is ${this.name} !`);
    }
}

let user1 = new User("Patate","15/07/89"); // Ici "this" remplace user1
let user2 = new User("Toto","15/07/89", 2);

user2.sayHello(); // Affiche dans la console "Hello, my name is Toto !"
```

attribut/propriété/paramètre/argument ?

* [extends](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Classes/extends)